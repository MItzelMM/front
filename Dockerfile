#Imagen Base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copiado de archivos
ADD build/es6-unbundled /app/build/es6-unbundled
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 30000

#Comando
CMD ["npm", "start"]
