  class MyMovi extends Polymer.Element {
    static get is() { return 'my-movi'; }

  static get properties() {
    return {
      data:{
          type: Object,
          value:{},
          computed: 'bienvenido()'
      },
      movis:{
          type: Object,
          value:{}
      },
      datamovi:{
          type: Object,
          value:{},
          computed: 'consultaClientes()'
      },
      datamovi2:{
          type: Object,
          value:{},
      },
      cargoselec:{type:String, value:""},
      contratoselec:{type:String, value:""},
      mUser: {type: Boolean, value :false, notify:true},
      amigo:{
        type: String
      }
    };
  }
  constructor() {
    super();
  }
  bienvenido(){
    var ddata=document.querySelector("my-app").data;

  return ddata;
  }
  consultaClientes(){
    var conclien;
    conclien=this.Clientes();
  return conclien;
  }
  Clientes() {
          var request = new XMLHttpRequest();
          var cadena = " http://localhost:1987/clientes/";
          request.open("GET", cadena ,false);
          request.setRequestHeader("Accept","application/json");
          request.send();
          this.clientes = JSON.parse(request.responseText);

          return this.clientes;
        }
  elijo() {

  var seleccionClien =this.$.Opciones ;
  var opcion=this.$.Opciones.value ;
  if (opcion!="Elija para jugar") {
      var indice = seleccionClien.selectedIndex -1 ;
      this.datamovi2=   this.datamovi[indice].contratos;
      this.movis=this.datamovi[indice].Movimientos;

     //this.$.resultadoEleccion.innerHTML = crea;
  }
      var indice = seleccionClien.selectedIndex -1 ;
  this.datamovi2=   this.datamovi[indice].contratos;

}
  cancelar() {

      this.movis= null;
      this.datamovi2= null;
      this.datamovi= null;
      this.cargoselec="";
      this.contratoselec="";
      this.amigo="";

      document.querySelector("my-app").page ='my-view3';

    }
    creaMovimiento(){

        var descargo="";
        var nuevo= this.movis.length;
        this.movis.push({});
        this.movis[nuevo].contrato=this.contratoselec;
        this.movis[nuevo].fecha=this.$.fecha.value;
        this.movis[nuevo].importe=parseInt(this.$.importe.value) ;
        this.movis[nuevo].tipo=this.cargoselec;
        if(this.cargoselec=="A"){descargo="ABONO";}else{descargo="CARGO";}
        this.movis[nuevo].desTipo=descargo;
        this.movis[nuevo].descripcion=this.$.descripcion.value ;
        this.movis[nuevo].descripcion2="NA";
        var data = ' {"$set":{"Movimientos":'+JSON.stringify(this.movis)+'}}';


                var request = new XMLHttpRequest();
                request.withCredentials = true;
                request.addEventListener("readystatechange", function () {
                  if (this.readyState === 4) {
                    console.log(this.responseText);
                  }
                });
              var cadena = " http://localhost:1987/movi/"+this.$.Opciones.value;
              request.open("PUT", cadena , true);
              request.setRequestHeader("Accept","application/json");
              request.setRequestHeader("content-type", "application/json");
              request.setRequestHeader("cache-control", "no-cache");

              request.send(data);

              if (request.status==200){
                console.log('si paso!');

              }else{
                console.log(request.status);
              }
              document.querySelector("my-app").page ='my-view3';

    }
    okaz(){
            var opcion=this.$.Opciones2.value ;
            if (opcion!="Elija contrato") {
               //this.$.resultadoEleccion.innerHTML = crea;
               this.contratoselec= opcion;
            }

    }
    cambia_cargo(){
      var opcion=this.$.tcargo.value
      if (opcion!="Elija para jugar") {
          this.cargoselec=   opcion;

         //this.$.resultadoEleccion.innerHTML = crea;
      }
    }


}
  window.customElements.define(MyMovi.is, MyMovi);
