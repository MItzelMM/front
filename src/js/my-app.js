Polymer.setPassiveTouchGestures(true);

class MyApp extends Polymer.Element {
  static get is() { return 'my-app'; }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      data:{
          type: Object,
          value:{}
      },
      routeData: Object,
      subroute: Object,
      rootPath: String,
      login: {type: Boolean, value :false, notify:true},
    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)',
    ];
  }

  _routePageChanged(page) {
    // If no page was found in the route data, page will be an empty string.
    // Default to 'view1' in that case.
    this.page = page || 'my-login';

    // Close a non-persistent drawer when the page & route are changed.

  }
  _pageChanged(page) {
    // Load page import on demand. Show 404 page if fails
    if(page=='components'){
      page='my-login'
    }
    const resolvedPageUrl = this.resolveUrl(  page+'.html');

    Polymer.importHref(
        resolvedPageUrl,
        null,
        this._showPage404.bind(this),
        true);
  }

logwwin() {
document.querySelector("my-app").page ='my-login';

}
  _showPage404() {
    this.page = 'my-login';
  }
  salir(){
    document.querySelector("my-app").data=null;
    document.querySelector("my-app").login=false;
    document.querySelector("my-app").page ='my-login';
  }
  editaDatos(){
    document.querySelector("my-app").page ='my-datos';
  }
  consulta(){
    document.querySelector("my-app").page ='my-movimientos';
  }
  saldos(){
    document.querySelector("my-app").page ='my-saldos';
  }
  alta(){
    document.querySelector("my-app").page ='my-alta';
  }
  movi(){
    document.querySelector("my-app").page ='my-movi';
  }

}

window.customElements.define(MyApp.is, MyApp);
